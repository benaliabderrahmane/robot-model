#include <catch2/catch.hpp>

#include <umrob/robot_model.h>

#include <fmt/format.h>
#include <fmt/ostream.h>

#include <cmath>
#include <random>
#include <fstream>

enum class Hand { Right, Left };

Eigen::Affine3d getHandPose(Hand hand, double j1, double j2) {
    double l1 = 0.5, l2 = 0.5;
    if (hand == Hand::Left) {
        l1 = -l1;
        l2 = -l2;
    }

    Eigen::Affine3d pose;

    pose.translation().x() = l1 * std::cos(j1) + l2 * std::cos(j1 + j2);
    pose.translation().y() = l1 * std::sin(j1) + l2 * std::sin(j1 + j2);
    pose.translation().z() = 0;

    pose.linear() =
        Eigen::AngleAxisd(j1 + j2, Eigen::Vector3d::UnitZ()).toRotationMatrix();

    return pose;
}

Eigen::Matrix<double, 6, 2> getHandJacobian(Hand hand, double j1, double j2) {
    double l1 = 0.5, l2 = 0.5;
    if (hand == Hand::Left) {
        l1 = -l1;
        l2 = -l2;
    }

    Eigen::Matrix<double, 6, 2> jacobian;
    jacobian.setZero();

    jacobian(0, 0) = -l1 * std::sin(j1) - l2 * std::sin(j1 + j2);
    jacobian(0, 1) = -l2 * std::sin(j1 + j2);

    jacobian(1, 0) = l1 * std::cos(j1) + l2 * std::cos(j1 + j2);
    jacobian(1, 1) = l2 * std::cos(j1 + j2);

    jacobian(5, 0) = 1;
    jacobian(5, 1) = 1;

    return jacobian;
}

constexpr auto planar_robot =
    R"(<robot name="PlanarRobot">
    <link name="root" />
    <link name="b1_right" />
    <link name="b2_right" />
    <link name="tcp_right" />
    <link name="b1_left" />
    <link name="b2_left" />
    <link name="tcp_left" />
    <joint name="j1_right" type="continuous">
        <parent link="root"/>
        <child link="b1_right"/>
        <origin rpy="0 0 0" xyz="0 0 0"/>
        <axis xyz="0 0 1"/>
    </joint>
    <joint name="j2_right" type="continuous">
        <parent link="b1_right"/>
        <child link="b2_right"/>
        <origin rpy="0 0 0" xyz="0.5 0 0"/>
        <axis xyz="0 0 1"/>
    </joint>
    <joint name="j3_right" type="fixed">
        <parent link="b2_right"/>
        <child link="tcp_right"/>
        <origin rpy="0 0 0" xyz="0.5 0 0"/>
    </joint>
    <joint name="j1_left" type="continuous">
        <parent link="root"/>
        <child link="b1_left"/>
        <origin rpy="0 0 0" xyz="0 0 0"/>
        <axis xyz="0 0 1"/>
    </joint>
    <joint name="j2_left" type="continuous">
        <parent link="b1_left"/>
        <child link="b2_left"/>
        <origin rpy="0 0 0" xyz="-0.5 0 0"/>
        <axis xyz="0 0 1"/>
    </joint>
    <joint name="j3_left" type="fixed">
        <parent link="b2_left"/>
        <child link="tcp_left"/>
        <origin rpy="0 0 0" xyz="-0.5 0 0"/>
    </joint>
</robot>
)";

umrob::RobotModel getNaoArmsModel() {
    std::ifstream model_file("../bin/share/robot-model/models/nao_arms.urdf");
    if (not model_file.is_open()) {
        fmt::print(stderr, "Failed to open the URDF file\n");
        std::exit(-1);
    }

    std::string urdf((std::istreambuf_iterator<char>(model_file)),
                     std::istreambuf_iterator<char>());

    return umrob::RobotModel(urdf);
}

TEST_CASE("hand pose") {
    auto model = umrob::RobotModel(planar_robot);

    std::random_device rd;
    std::uniform_real_distribution<double> dist(-M_PI, M_PI);
    for (size_t i = 0; i < 100; i++) {
        const double j1_right = dist(rd);
        const double j2_right = dist(rd);
        const double j1_left = dist(rd);
        const double j2_left = dist(rd);
        model.jointPosition("j1_right") = j1_right;
        model.jointPosition("j2_right") = j2_right;
        model.jointPosition("j1_left") = j1_left;
        model.jointPosition("j2_left") = j2_left;
        model.update();

        auto pose_right = model.linkPose("tcp_right");
        auto expected_pose_right = getHandPose(Hand::Right, j1_right, j2_right);

        REQUIRE(pose_right.matrix().isApprox(expected_pose_right.matrix()));

        auto pose_left = model.linkPose("tcp_left");
        auto expected_pose_left = getHandPose(Hand::Left, j1_left, j2_left);

        REQUIRE(pose_left.matrix().isApprox(expected_pose_left.matrix()));
    }
}

TEST_CASE("hand jacobian") {
    auto model = umrob::RobotModel(planar_robot);

    std::random_device rd;
    std::uniform_real_distribution<double> dist(-M_PI, M_PI);
    for (size_t i = 0; i < 100; i++) {
        const double j1_right = dist(rd);
        const double j2_right = dist(rd);
        const double j1_left = dist(rd);
        const double j2_left = dist(rd);
        model.jointPosition("j1_right") = j1_right;
        model.jointPosition("j2_right") = j2_right;
        model.jointPosition("j1_left") = j1_left;
        model.jointPosition("j2_left") = j2_left;
        model.update();

        auto jacobian_right = model.linkJacobian("tcp_right");
        auto expected_jacobian_right =
            getHandJacobian(Hand::Right, j1_right, j2_right);

        REQUIRE(
            jacobian_right.matrix.isApprox(expected_jacobian_right.matrix()));
        
        auto jacobian_left = model.linkJacobian("tcp_left");
        auto expected_jacobian_left =
            getHandJacobian(Hand::Left, j1_left, j2_left);

        REQUIRE(jacobian_left.matrix.isApprox(expected_jacobian_left.matrix()));
    }

    auto jacobian_right = model.linkJacobian("tcp_right");
    REQUIRE(jacobian_right.joints_name ==
            std::vector<std::string>{"j1_right", "j2_right"});

    auto jacobian_left = model.linkJacobian("tcp_left");
    REQUIRE(jacobian_left.joints_name ==
            std::vector<std::string>{"j1_left", "j2_left"});
}

/*
TEST_CASE("nao arms") {
    auto model = getNaoArmsModel();

    SECTION("All zero") {
        model.jointPosition("LShoulderPitch") = 0;
        model.jointPosition("LShoulderRoll") = 0;
        model.jointPosition("LElbowYaw") = 0;
        model.jointPosition("LElbowRoll") = 0;
        model.jointPosition("LWristYaw") = 0;
        model.jointPosition("LHand") = 0;
        model.jointPosition("RShoulderPitch") = 0;
        model.jointPosition("RShoulderRoll") = 0;
        model.jointPosition("RElbowYaw") = 0;
        model.jointPosition("RElbowRoll") = 0;
        model.jointPosition("RWristYaw") = 0;
        model.jointPosition("RHand") = 0;

        // clang-format off
        Eigen::Matrix4d expected_r_gripper_pose;
        expected_r_gripper_pose << 
            1, 0, 0, 0.2187, 
            0, 1, 0, -0.113, 
            0, 0, 1, 0.08769,
            0, 0, 0, 1;

        Eigen::Matrix4d expected_l_gripper_pose;
        expected_l_gripper_pose << 
            1, 0, 0, 0.2187, 
            0, 1, 0, 0.113, 
            0, 0, 1, 0.08769,
            0, 0, 0, 1;

        Eigen::Matrix<double, 6, 6> expected_r_gripper_jacobian;
        expected_r_gripper_jacobian <<
            -0.01231,   0.015,    0,        0,        0,        0,
            0,          0.2187,   0.01231,  0.1137,   0.01231,  0,
            -0.2187,    0,        0,        0,        0,        0,
            0,          0,        1,        0,        1,        1,
            1,          0,        0,        0,        0,        0,
            0,          1,        0,        1,        0,        0;

        Eigen::Matrix<double, 6, 6> expected_l_gripper_jacobian;
        expected_l_gripper_jacobian <<
            -0.01231,   -0.015,   0,        0,        0,        0,
            0,          0.2187,   0.01231,  0.1137,   0.01231,  0,
            -0.2187,    0,        0,        0,        0,        0,
            0,          0,        1,        0,        1,        1,
            1,          0,        0,        0,        0,        0,
            0,          1,        0,        1,        0,        0;
        // clang-format on

        model.update();

        auto r_gripper_pose = model.linkPose("r_gripper");

        fmt::print("r_gripper_pose:\n{}\n", r_gripper_pose.matrix());
        fmt::print("expected_r_gripper_pose:\n{}\n", expected_r_gripper_pose);
        
        REQUIRE(r_gripper_pose.matrix().isApprox(expected_r_gripper_pose));

        auto r_gripper_jacobian = model.linkJacobian("r_gripper");
        REQUIRE(
            r_gripper_jacobian.matrix.isApprox(expected_r_gripper_jacobian));

        auto l_gripper_pose = model.linkPose("l_gripper");
        REQUIRE(l_gripper_pose.matrix().isApprox(expected_l_gripper_pose));

        auto l_gripper_jacobian = model.linkJacobian("l_gripper");
        REQUIRE(
            l_gripper_jacobian.matrix.isApprox(expected_l_gripper_jacobian));
    }

    SECTION("All ones") {
        model.jointPosition("LShoulderPitch") = 1;
        model.jointPosition("LShoulderRoll") = 1;
        model.jointPosition("LElbowYaw") = 1;
        model.jointPosition("LElbowRoll") = 1;
        model.jointPosition("LWristYaw") = 1;
        model.jointPosition("LHand") = 1;
        model.jointPosition("RShoulderPitch") = 1;
        model.jointPosition("RShoulderRoll") = 1;
        model.jointPosition("RElbowYaw") = 1;
        model.jointPosition("RElbowRoll") = 1;
        model.jointPosition("RWristYaw") = 1;
        model.jointPosition("RHand") = 1;

        // clang-format off
        Eigen::Matrix4d expected_r_gripper_pose;
        expected_r_gripper_pose << 
            0.546846,  0.759536, -0.352228, 0.0941235,
            0.700296, -0.184387,  0.689628, 0.0591968,
            0.458851, -0.623785, -0.632731, 0.104482,
            0,        0,         0,         1;

        Eigen::Matrix4d expected_l_gripper_pose;
        expected_l_gripper_pose << 
            0.546846,  0.759536, -0.352228, 0.0804841,
            0.700296, -0.184387,  0.689628, 0.271406,
            0.458851, -0.623785, -0.632731, 0.125724,
            0,        0,         0,         1;

        Eigen::Matrix<double, 6, 6> expected_r_gripper_jacobian;
        expected_r_gripper_jacobian <<
            0.00448195,  -0.0849338,   0.0878628, -0.00518676,  0.00870032,           0,
                    0,   0.0470837,  -0.0441015,  -0.0698282, -0.00836989,           0,
            -0.0941235,    0.132277,  -0.0252077,   0.0901782,  0.00240528,           0,
                    0,    0.841471,    0.291927,    0.837222,    0.546846,    0.546846,
                    1,           0,    0.841471,   -0.454649,    0.700296,    0.700296,
                    0,    0.540302,   -0.454649,   -0.303897,    0.458851,    0.458851;

        Eigen::Matrix<double, 6, 6> expected_l_gripper_jacobian;
        expected_l_gripper_jacobian <<
            0.0257242,   -0.0936916,    0.0878628,  -0.00518676,   0.00870032, -1.05407e-17,
                    0,    0.0218396,   -0.0441015,   -0.0698282,  -0.00836989,  2.55888e-18,
            -0.0804841,     0.145916,   -0.0252077,    0.0901782,   0.00240528,  8.65675e-18,
                    0,     0.841471,     0.291927,     0.837222,     0.546846,     0.546846,
                    1,            0,     0.841471,    -0.454649,     0.700296,     0.700296,
                    0,     0.540302,    -0.454649,    -0.303897,     0.458851,     0.458851;
        // clang-format on

        model.update();

        auto r_gripper_pose = model.linkPose("r_gripper");
        fmt::print("r_gripper_pose:\n{}\n", r_gripper_pose.matrix());
        fmt::print("expected_r_gripper_pose:\n{}\n", expected_r_gripper_pose);
        REQUIRE(
           r_gripper_pose.matrix().isApprox(expected_r_gripper_pose, 1e-6));

        auto r_gripper_jacobian = model.linkJacobian("r_gripper");
        REQUIRE(r_gripper_jacobian.matrix.isApprox(expected_r_gripper_jacobian,
                                                  1e-6));

        auto l_gripper_pose = model.linkPose("l_gripper");
        REQUIRE(
            l_gripper_pose.matrix().isApprox(expected_l_gripper_pose, 1e-6));

        auto l_gripper_jacobian = model.linkJacobian("l_gripper");
        REQUIRE(l_gripper_jacobian.matrix.isApprox(expected_l_gripper_jacobian,
                                                  1e-6));
    }
}
*/


TEST_CASE("dof count") {
    SECTION("planar robot") {
        auto model = umrob::RobotModel(planar_robot);

        REQUIRE(model.degreesOfFreedom() == 4);
    }

    SECTION("nao arms") {
        auto model = getNaoArmsModel();

        REQUIRE(model.degreesOfFreedom() == 12);
    }
}

TEST_CASE("joint index") {
    auto model = umrob::RobotModel(planar_robot);

    std::vector<bool> used_indexes(model.degreesOfFreedom(), false);
    for (auto [name, pos] : model.jointsPosition()) {
        size_t index = model.jointIndex(name);
        used_indexes.at(index) = true;
        REQUIRE(model.jointNameByIndex(index) == name);
    }

    REQUIRE(std::all_of(used_indexes.begin(), used_indexes.end(),
                        [](bool v) { return v; }));
}