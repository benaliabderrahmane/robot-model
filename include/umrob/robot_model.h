//! \file robot_model.h
//! \author Benjamin Navarro
//! \brief Declaration of the RobotModel class
//! \date 08-2020

#pragma once

#include <Eigen/Dense>
#include <urdf_parser/urdf_parser.h>

#include <limits>
#include <map>
#include <string>
#include <type_traits>

namespace umrob {

//! \brief Hold the position, velocity and effort limits of a joint
//!
struct JointLimits {
    //! \brief Minimum joint position (rad)
    double min_position{-std::numeric_limits<double>::infinity()};
    //! \brief Maximum joint position (rad)
    double max_position{std::numeric_limits<double>::infinity()};
    //! \brief Maximum joint velocity (rad/s)
    double max_velocity{std::numeric_limits<double>::infinity()};
    //! \brief Maximum joint effort (Nm)
    double max_effort{std::numeric_limits<double>::infinity()};
};

//! \brief Hold a 6xDofs Jacobian matrix and its associated list of joints
//!
struct Jacobian {
    //! \brief The Jacobian matrix
    Eigen::Matrix<double, 6, Eigen::Dynamic> matrix;
    //! \brief Names of the joints associated with each column of the matrix, in
    //! the same order
    std::vector<std::string> joints_name;
    //! \brief Joints involved in the Jacobian
    std::vector<urdf::JointConstSharedPtr> joints_chain;
};

//! \brief Handle computations related to a robot model: forward kinematics,
//! jacobian extraction, etc
//!
class RobotModel {
public:
    //! \brief Helper class providing an iteratable view on a container
    //!
    //! \tparam ContainerT Container to wrap (deduced with CTAD)
    template <typename ContainerT> class ContainerView {
    public:
        explicit ContainerView(ContainerT& container) : container_{container} {
        }

        typename ContainerT::const_iterator begin() const {
            return container_.begin();
        }

        template <typename T = ContainerT>
        std::enable_if_t<not std::is_const_v<T>, typename ContainerT::iterator>
        begin() {
            return container_.begin();
        }

        typename ContainerT::const_iterator end() const {
            return container_.end();
        }

        template <typename T = ContainerT>
        std::enable_if_t<not std::is_const_v<T>, typename ContainerT::iterator>
        end() {
            return container_.end();
        }

    private:
        ContainerT& container_;
    };

    //! \brief Construct a RobotModel using a URDF description of the robot
    //!
    //! \param urdf model description in URDF format
    explicit RobotModel(const std::string& urdf);

    //! \brief Use the current joint state to update all links' pose
    void update();

    //! \brief Provide a read/write reference to the position of a joint
    //!
    //! \param name Name of the joint
    //! \return double& Its position in radians
    double& jointPosition(const std::string& name);

    //! \brief ¨Provide an iteratable object to read all joints' position
    //!
    //! \return ContainerView All joints' position
    auto jointsPosition() const {
        return ContainerView{joints_position_};
    }

    //! \brief ¨Provide an iteratable object to read/write all joints' position
    //!
    //! \return ContainerView All joints' position
    auto jointsPosition() {
        return ContainerView{joints_position_};
    }

    //! \brief Provide the index of a joint based on its name
    //!
    //! \param name The joint
    //! \return size_t Its index
    size_t jointIndex(const std::string& name) const;

    //! \brief Provide the name of a joint based on its index
    //!
    //! \param index The joint
    //! \return const std::string& Its name
    const std::string& jointNameByIndex(size_t index) const;

    //! \brief Provide a read only reference to a link's pose.
    //!
    //! \details The referenced pose will be updated when update() is called so
    //! a single call to linkPose is sufficient if the returned referenced is
    //! stored.
    //!
    //! \param link_name The link to get the pose of
    //! \return const Eigen::Affine3d& The link pose in the root frame
    const Eigen::Affine3d& linkPose(const std::string& link_name) const;

    //! \brief Get the Jacobian, in the root frame, of the given link.
    //!
    //! \details The referenced Jacobian will be updated when update() is called
    //! so a single call to linkJacobian is sufficient if the returned
    //! referenced is stored.
    //!
    //! \param link_name Name of the link
    //! \return const Jacobian& The link's Jacobian in the root frame
    const Jacobian& linkJacobian(const std::string& link_name);

    //! \brief Get the limits of the given joint
    //!
    //! \param joint_name The joint
    //! \return const JointLimits& Its limits
    const JointLimits& jointLimits(const std::string& joint_name) const;

    //! \brief Gives the robot number of degrees of freedom. Might be different
    //! from the number of joints
    //!
    //! \return size_t Number of degrees of freedom
    size_t degreesOfFreedom() const;

    //! \brief Extracts the joints involed with the Jacobian of a link, i.e
    //! joints from the root to the link, in that order.
    //!
    //! \param link_name The link
    //! \return std::vector<urdf::JointConstSharedPtr> Vector of joints invoved
    //! in its Jacobian
    std::vector<urdf::JointConstSharedPtr>
    jacobianJointChain(const std::string& link_name) const;

    //! \brief Check if the model has the given joint
    //!
    //! \param name The joint
    //! \return bool True if it exists, false otherwise
    bool hasJoint(const std::string& name) const;

    //! \brief Check if the model has the given link
    //!
    //! \param name The link
    //! \return bool True if it exists, false otherwise
    bool hasLink(const std::string& name) const;

private:
    urdf::ModelInterfaceSharedPtr model_;
    std::map<std::string, double> joints_position_;
    std::map<std::string, Eigen::Affine3d> links_pose_;
    std::map<std::string, JointLimits> joints_limits_;
    std::map<std::string, Jacobian> links_jacobian_;
    size_t degrees_of_freedom_{0};
};

} // namespace umrob