# robot-model project

This project provides a library to compute the forward kinematics and link Jacobians of any robot using its URDF model.

## Objective

Using the [urdfdomcpp](https://github.com/BenjaminNavarro/urdfdomcpp) (URDF parser) and [Eigen](eigen.tuxfamily.org/) (linear algebra) libraries, implement the algorithms required to extract the pose and Jacobian of any robot link (body) based on the current joints' position.

A code skeleton is provided. Fill all functions inside *src/robot_model/robot_model.cpp* that are tagged with a `//TODO implement` comment. While implementing a function, remove any `[[maybe_unused]]` attribute as they are present only to avoid warnings.

You must **not** modify the existing `RobotModel` class public interface (functions, variables, types), but you are free to add any member function or variable. This makes sure that all unit tests in *tests/forward_kinematics/forward_kinematics.cpp* can be compiled without any modification. If you add new public member functions, please try to add unit tests for them.

For simplification, consider that all joints types are either `fixed`, `revolute` or `continuous`.

## Bonus objective

If you managed to complete everything that is listed above in time, you can try to handle other joint types, starting with `prismatic` joints. The other two, `planar` and `floating`, require modifications to the public interface since they have more than one degree of freedom.

Any additional joint type that is properly implemented will grant you bonus points.

## Type and orientation conversions

### urdfdomcp -> Eigen

```cpp
Eigen::Vector3d to_vector3d(urdf::Vector3 v) {
    return Eigen::Vector3d(v.x, v.y, v.z);
};

Eigen::Quaterniond to_quat(urdf::Rotation q) {
    return Eigen::Quaterniond(q.w, q.x, q.y, q.z);
};
```

### urdf::Pose to Eigen::Affine3d

```cpp
urdf::Pose pose = ...;
Eigen::Affine3d affine;
affine.translation() = to_vector3d(pose.position);
affine.linear() = to_quat(pose.rotation).toRotationMatrix();
```

### AngleAxis to Rotation matrix

```cpp
Eigen::Vector3d axis = ...;
double angle = ...;
Eigen::Matrix3d = Eigen::AngleAxisd(angle, axis).toRotationMatrix()
```

## urdfdomcpp API

Here are some useful functions and types from urdfdomcpp to know to complete this project:
 - `urdf::parseURDF(std::string) -> urdf::ModelInterfaceSharedPtr`: take a URDF model and parse it to build an internal representation of it that you can query after
 - `urdf::ModelInterface`: hold all the information about a model (its name, joints, links, etc)
   - `getRoot() -> urdf::LinkConstSharedPtr`: get a pointer to a link
   - `getLink(std::string) -> urdf::LinkConstSharedPtr`: get a pointer to a link
 - `urdf::Link`: represent a link inside the model. Has the following useful attributes:
   - `name`: name of the link
   - `child_joints`: an `std::vector` of `urdf::JointConstSharedPtr` (`std::shared_ptr` to `urdf::Joint`s) which contains all the child joints attached to this link
   - `parent_joint`: a pointer to the parent joint of the link
 - `urdf::Joint`: represent a joint inside the model. Has the following useful attributes:
   - `name`: name of the joint
   - `type`: type of joint (e.g `urdf::Joint::REVOLUTE`, `urdf::Joint::CONTINUOUS` or `urdf::Joint::FIXED`)
   - `axis`: an `urdf::Vector3` representing the axis of the joint
   - `child_link_name`: name of the child link
   - `parent_link_name`: name of the parent link
   - `parent_to_joint_origin_transform`: an `urdf::Pose` representing the transform from the parent link frame to the joint frame
   - `limits`: pointer to the limits of a joint (null if not limits are defined in the URDF)
      - `lower`: the lower position limit
      - `upper`: the upper position limit
      - `velocity`: the maximum velocity (positive)
      - `effort`: the maximum effort (positive)
 - `urdf::Pose`: an pose in 3D space
    - `translation`: a `urdf::Vector3` holding the translation part
      - `x`: x component
      - `y`: y component
      - `z`: z component
    - `rotation`: a `urdf::Rotation` holding the rotation part, represented by quaternion
      - `x`: x component
      - `y`: y component
      - `z`: z component
      - `w`: w (real) component




## How to build and test the code
From the `build` folder:
1. Configure the project with tests enabled: `cmake -DENABLE_TESTING=ON ..` (you can remove the `-DENABLE_TESTING=ON` part if you don't care about testing)extract Ja
2. Build the code: `cmake --build . --parallel`
3. To run the tests: `ctest`
4. To create a local reusable Conan package: `conan create .. user/channel` (e.g `conan create .. johndoe/stable`)

## Tips and tricks
* When you add new source files, don't forget to list them in the corresponding `CMakeLists.txt` file otherwise they won't be built
* If your IDE doesn't automatically format your code, you can run the `format.sh` script at the root of the project to do so
* You can generate the Doxygen documentation using the `doc` target: `cmake --build . --target doc`. Then just open the `build/docs/index.html` file in your browser to access it.
* You can generate the a test coverage report using the `coverage` target: `cmake --build . --target doc`. Then just open the `build/coverage/index.html` file in your browser to access it.
* To enforce good coding practice, the compiler is configured to generate more warnings and to treat all warnings as errors. Both of these aspects can be disabled using the `MORE_WARNINGS` and `WARNINGS_AS_ERRORS` CMake options.
* If a test fails, you can find its console output in `build/Testing/Temporary/LastTest.log`. Alternatively, you can run the test manually, e.g `./bin/my_test`. You can pass the `-s` option after the executable name to list successful tests
