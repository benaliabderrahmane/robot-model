#include "umrob/robot_model.h"

#include <fmt/format.h>
#include <fmt/ostream.h>

constexpr auto planar_robot =
    R"(<robot name="PlanarRobot">
    <link name="root" />
    <link name="b1" />
    <link name="b2" />
    <link name="tcp" />
    <joint name="j1" type="continuous">
        <parent link="root"/>
        <child link="b1"/>
        <origin rpy="0 0 0" xyz="0 0 0"/>
        <axis xyz="0 0 1"/>
    </joint>
    <joint name="j2" type="continuous">
        <parent link="b1"/>
        <child link="b2"/>
        <origin rpy="0 0 0" xyz="0.5 0 0"/>
        <axis xyz="0 0 1"/>
    </joint>
    <joint name="j3" type="fixed">
        <parent link="b2"/>
        <child link="tcp"/>
        <origin rpy="0 0 0" xyz="0.5 0 0"/>
    </joint>
</robot>
)";

int main(int argc, char* argv[]) {
    // Construct a robot model with a given URDF description
    auto model = umrob::RobotModel(planar_robot);

    // Read the joints positions from the apps parameters if given
    if (argc > 1) {
        model.jointPosition("j1") = atof(argv[1]);
    } else {
        model.jointPosition("j1") = M_PI / 2.;
    }
    if (argc > 2) {
        model.jointPosition("j2") = atof(argv[2]);
    } else {
        model.jointPosition("j2") = M_PI / 2.;
    }

    fmt::print("Joint positions:\n");
    for (auto it : model.jointsPosition()) {
        fmt::print("{}: {}\n", it.first, it.second);
    }

    // Take the current joint state and use it to update all the links' pose
    // (forward kinematics)
    model.update();

    // Now any link's pose or jacobian can be retrieved from the model
    fmt::print("root pose:\n{}\n", model.linkPose("root").matrix());
    fmt::print("b1 pose:\n{}\n", model.linkPose("b1").matrix());
    fmt::print("b2 pose:\n{}\n", model.linkPose("b2").matrix());
    fmt::print("tcp pose:\n{}\n", model.linkPose("tcp").matrix());

    auto jacobian = model.linkJacobian("tcp");
    fmt::print("tcp jacobian:\n{}\n", jacobian.matrix);
}
