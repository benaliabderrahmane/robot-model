#include <fmt/core.h>
#include <umrob/robot_model.h>

#include <urdf_model/pose.h>
#include <urdf_parser/urdf_parser.h>
#include <fmt/format.h>
#include <fmt/ostream.h>

#include <stdexcept>

/////added for testing////
#include <iostream>

using namespace urdf;
using namespace std;
/////added for testing////


Eigen::Vector3d to_vector3d(urdf::Vector3 v) {
    return Eigen::Vector3d(v.x, v.y, v.z);
};

Eigen::Quaterniond to_quat(urdf::Rotation q) {
    return Eigen::Quaterniond(q.w, q.x, q.y, q.z);
};

namespace umrob {
RobotModel::RobotModel([[maybe_unused]] const std::string& urdf) {
    // TODO: implement
    std::cout << "\n\n--- Loading URDF C++ model ---\n\n";
    model_ = urdf::parseURDF(urdf);
    std::cout << "\n\n--- URDF C++ model loaded ---\n\n";

    // "joint->type" returns the type "urdf::Joint"  
    /// \brief     type_       meaning of axis_
    /// ------------------------------------------------------
    ///            UNKNOWN     unknown type
    ///            REVOLUTE    rotation axis
    ///            PRISMATIC   translation axis
    ///            FLOATING    N/A
    ///            PLANAR      plane normal axis
    ///            FIXED       N/A


    auto joints = model_->joints_;

    for (map<string,JointSharedPtr>::iterator joint = joints.begin();
        joint != joints.end(); joint++) 
    {
        if (joint->second->type != 6)
        {
            joints_position_[joint->second->name] = 0;
            // Same for joint limits, which belongs to type JointLimits
            JointLimits limits;
            if (joint->second->limits) 
            {
                limits.min_position = joint->second->limits->lower;
                limits.max_position = joint->second->limits->upper;
                limits.max_velocity = joint->second->limits->velocity;
                limits.max_effort = joint->second->limits->effort;
                if (joints_position_[joint->second->name]>limits.max_position)
                    joints_position_[joint->second->name]=limits.max_position;
                if (joints_position_[joint->second->name]<limits.min_position)
                    joints_position_[joint->second->name]=limits.min_position;
            }
            joints_limits_[joint->second->name] = limits;

        }
    }

    auto links = model_->links_;
    for (auto link = links.begin();link != links.end(); link++) 
    {
        Eigen::Affine3d joint_pose;
        links_pose_[link->second->name] = joint_pose;
    }

    for (map<string,JointSharedPtr>::iterator joint = joints.begin();
    joint != joints.end(); joint++) 
    {
        auto origin = joint->second->parent_to_joint_origin_transform;
        
        Eigen::Affine3d joint_pose;
        joint_pose.translation() = to_vector3d(origin.position);
        joint_pose.linear() = to_quat(origin.rotation).toRotationMatrix(); 

        auto axis = to_vector3d(joint->second->axis);
        auto joint_transform = Eigen::Affine3d::Identity();
        if (joint->second->type != 6)
        joint_transform.linear() = Eigen::AngleAxisd(joints_position_.at(joint->second->name), axis).toRotationMatrix(); 
        
        joint_pose = joint_pose*joint_transform;
        
        links_pose_[joint->second->child_link_name] = joint_pose;
    }
}

double& RobotModel::jointPosition(const std::string& name) {
    return joints_position_.at(name);
}

size_t RobotModel::jointIndex([[maybe_unused]] const std::string& name) const {
    
    size_t jointIndex = distance(joints_position_.begin(), joints_position_.find(name)); 
    return jointIndex;
}

const std::string& RobotModel::jointNameByIndex([
    [maybe_unused]] size_t index) const 
    {
        auto it = joints_position_.begin();
        std::advance(it, index);
        return it->first; 
    }

void RobotModel::update() {
    map<string,JointSharedPtr> joints = model_->joints_;
    for (map<string,JointSharedPtr>::iterator joint = joints.begin();
    joint != joints.end(); joint++) 
    {
        JointLimits limits;
        if (joints_position_[joint->second->name]>limits.max_position)
            joints_position_[joint->second->name]=limits.max_position;
        if (joints_position_[joint->second->name]<limits.min_position)
            joints_position_[joint->second->name]=limits.min_position;

        auto origin = joint->second->parent_to_joint_origin_transform;

        Eigen::Affine3d joint_pose;
        joint_pose.translation() = to_vector3d(origin.position);
        joint_pose.linear() = to_quat(origin.rotation).toRotationMatrix(); 

        Eigen::Vector3d axis = to_vector3d(joint->second->axis);
        Eigen::Affine3d joint_transform = Eigen::Affine3d::Identity();
        if (joint->second->type != 6)
        joint_transform.linear() = Eigen::AngleAxisd(joints_position_.at(joint->second->name), axis).toRotationMatrix(); 

        if ((joint->second->parent_link_name)!= "root") 
        { 
            joint_pose = (links_pose_.at(joint->second->parent_link_name))*joint_pose*joint_transform;
        } 
        else 
        {
            joint_pose = joint_pose*joint_transform;
        }

        links_pose_[joint->second->child_link_name] = joint_pose;
    }
}

const Eigen::Affine3d&
RobotModel::linkPose(const std::string& link_name) const {
    return links_pose_.at(link_name);
}

const Jacobian& RobotModel::linkJacobian([
    [maybe_unused]] const std::string& link_name) {
    Jacobian jac;
    auto link = model_->getLink(link_name);
    auto presentLink = link;
    while (true) 
    {
        if (presentLink->parent_joint->type != 6)
        {
        jac.joints_name.push_back(presentLink->parent_joint->name);
        jac.joints_chain.push_back(model_->getJoint(presentLink->parent_joint->name)); 
        }

        presentLink = presentLink->getParent();

        if (presentLink == model_->getRoot())
            break;
    }

    std::reverse(jac.joints_chain.begin(),jac.joints_chain.end()); 
    std::reverse(jac.joints_name.begin(),jac.joints_name.end()); 


    for (std::vector<urdf::JointConstSharedPtr>::iterator it = jac.joints_chain.begin(); it != jac.joints_chain.end(); ++it) {
        Eigen::Vector3d axR = to_vector3d((*it)->axis);
        std::ptrdiff_t rotAx;
        double maxOfV = axR.maxCoeff(&rotAx);
        Eigen::Vector3d zi = links_pose_.at((*it)->child_link_name).matrix().block(0, rotAx, 3, 1);
        Eigen::Vector3d li = links_pose_.at(link_name).matrix().block(0, 3, 3, 1) - links_pose_.at((*it)->child_link_name).matrix().block(0, 3, 3, 1);
        rotAx = maxOfV;
        auto matCros = zi.cross(li);

        Eigen::Matrix<double, 6, 1> Ji;
        Ji << matCros, zi;

        jac.matrix.conservativeResize(6, jac.matrix.cols()+1);
        jac.matrix.col(jac.matrix.cols()-1) << Ji;
    }
    links_jacobian_[link_name] = jac;
    return links_jacobian_.at(link_name);
}

const JointLimits&
RobotModel::jointLimits(const std::string& joint_name) const {
    return joints_limits_.at(joint_name);
}

size_t RobotModel::degreesOfFreedom() const {
    auto root = model_->getRoot();
    auto joints = root->child_joints;

    auto v = model_->joints_;
    
    size_t degrees_of_freedom_ = 0;

    for (map<string,JointSharedPtr>::iterator joint = v.begin();
        joint != v.end(); joint++) 
    {
        if (joint->second->type != 6)
        // dof is the number of joints that aren't fixed thus this condition
        degrees_of_freedom_++;
    }

    return degrees_of_freedom_;
}

std::vector<urdf::JointConstSharedPtr> RobotModel::jacobianJointChain([
    [maybe_unused]] const std::string& link_name) const {
    std::vector<urdf::JointConstSharedPtr> joint_chain;
    return joint_chain;
}

} // namespace umrob