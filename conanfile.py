from conans import ConanFile, CMake, tools


class RobotModelConan(ConanFile):
    name = "robot-model"
    version = "1.0"
    license = "BSD"
    author = "Benjamin Navarro <navarro.benjamin13@gmail.com>"
    url = "you-git-project-homepage.com"
    description = "Compute a robot forward kinematics based on its URDF model"
    topics = "C++", "Conan", "CMake", "Forward Kinematics", "URDF"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [
        True, False], "build_tests": [True, False]}
    default_options = {"shared": False, "fPIC": True, "build_tests": False}
    generators = "cmake"
    requires = "urdfdomcpp/1.0@bnavarro/stable", "fmt/7.0.1"
    exports_sources = "!.clangd*", "!.ccls-cache*", "!compile_commands.json", "*"

    def requirements(self):
        if self.options.build_tests:
            self.requires("cppcheck/2.4.1")
            self.requires("catch2/2.13.0")
            # add test dependencies here if needed

    def configure(self):
        if self.settings.compiler == 'Visual Studio':
            del self.options.fPIC

    def build(self):
        cmake = CMake(self)
        if self.options.build_tests:
            cmake.definitions["ENABLE_TESTING"] = True
        cmake.configure()
        cmake.build()
        if self.options.build_tests:
            cmake.test()
        cmake.install()

    def package(self):
        self.copy("*", src="bin", dst="bin", keep_path=True)
        self.copy("*robot_model.lib", dst="lib", keep_path=False)
        self.copy("*robot_model.dll", dst="bin", keep_path=False)
        self.copy("*robot_model.so", dst="lib", keep_path=False)
        self.copy("*robot_model.dylib", dst="lib", keep_path=False)
        self.copy("*robot_model.a", dst="lib", keep_path=False)
        self.copy("*", src="../include", dst="include", keep_path=True)

    def package_info(self):
        self.cpp_info.libs = ["robot_model"]
